/**
 * MascotaController
 *
 * @description :: Server-side logic for managing mascotas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	index: function (req, res, next) {
		var pers = null;
		Persona.find().exec(function (err, personas) {
		  if (err) return res.serverError(err);			
			pers = personas;
		});
		Mascota.find().populate('dueño').exec( function (err, mascotas) {
			res.view({
				mascotas: mascotas,
				personas: pers
			})
		})
	},

	create: function (req, res) {
		Mascota.create( req.params.all(), function success(err, mascota){
			if (err) return res.redirect('back');
			res.redirect('back');
		})
	},

	destroy: function (req, res, next) {
		Mascota.findOne(req.param('id'), function encontrado(err, per){
			if(err) return next(err);
			if(!per) return next('Mascota no existe');
			Mascota.destroy(req.param('id'), function borrado (err) {
				if(err) return next(err);
			})

			res.redirect('back');
		})
	},

	edit: function (req, res) {
		var pers = null;
		Persona.find().exec(function (err, personas) {
		  if (err) return res.serverError(err);			
			pers = personas;
		});
		Mascota.findOne({
		  id: req.param('id')
		})
		.populate('dueño')
		.exec(function (err, mascota) {
		  if (err) return res.serverError(err);
		  if (!mascota) return res.notFound('La mascota no existe.');
		
		  res.view({
		  	mascota: mascota,
		  	personas: pers
		  })
		});
	},

	update: function (req, res, next) {
		Mascota.update( req.param('id'), req.params.all(), function actualizado (err) {
			if (err){
				return next(err);
			} 
			res.redirect('persona/mascotas/'+req.param('dueño'));
		})
	},
	
};

