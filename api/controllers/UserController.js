/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	index: function (req, res, next) {
		User.find( function personas (err, users) {
			res.view( 'chat/index', {
				users: users
			})
		})
	},

	create: function (req, res) {
		User.create( req.params.all(), function success(err, user){
			if (err) return res.redirect('back');
			res.redirect('back');
		})
	},
};

