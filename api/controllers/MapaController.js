/**
 * MapaController
 *
 * @description :: Server-side logic for managing mapas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  index:function (req,res) {

      res.view('mapa/index');
  },

  conectar:function (req,res) {
    sails.sockets.join(req,'mapa',function (err) {
      if(err){return res.serverError(err);}

      return res.json({
        message: 'inicializado',
      });

    });
  },

  cambio:function (req,res) {
    var pun=req.param('punto');
    var sty=req.param('style');

    sails.sockets.broadcast('mapa','cambio',{punto:pun,style:sty});
  }

};

