/**
 * PersonaController
 *
 * @description :: Server-side logic for managing personas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	index: function (req, res, next) {
		Persona.find( function personas (err, personas) {
			res.view({
				personas: personas
			})
		})
	},

	create: function (req, res) {
		Persona.create( req.params.all(), function success(err, persona){
			if (err) return res.redirect('back');
			res.redirect('back');
		})
	},

	destroy: function (req, res, next) {
		Persona.findOne({
		  	id: req.param('id')
		}).exec(function (err, per){
		  	if (err) {
		  	  return res.serverError(err);
		  	}
		  	if (!per) {
		  	  return res.notFound('La persona no existe');
		  	}
		
		  	Persona.destroy({
			  id: req.param('id')
			}).exec(function (err){
			  if (err) {
			    return res.negotiate(err);
			  }
			  return res.redirect('/persona');
			});
		});
		/*Persona.findOne(req.param('id'), function encontrado(err, per){
			if(err) return next(err);
			if(!per) return next('Persona no existe');
			Persona.destroy(req.param('id'), function borrado (err) {
				if(err) return next(err);
			})

			res.redirect('back');
		})*/
	},

	edit: function (req, res) {
		Persona.findOne({
		  id: req.param('id')
		}).exec(function (err, persona) {
		  if (err) return res.serverError(err);
		  if (!persona) return res.notFound('La persona no existe.');
		
		  res.view({
		  	persona: persona
		  })
		});
	},

	update: function (req, res, next) {
		Persona.update( req.param('id'), req.params.all(), function actualizado (err) {
			if (err){
				return next(err);
			} 
			res.redirect('/persona');
		})
	},

	mascotas: function (req, res, next) {
		var pers = null;
		Persona.findOne({
		  id: req.param('id')
		}).exec(function (err, persona) {
		  if (err) return res.serverError(err);
		  if (!persona) return res.notFound('La persona no existe.');
			
			pers = persona;
		});
		Mascota.find({dueño: req.param('id')}, function getMascotas (err, mascotas) {
			if (err){
				return next(err);
			}
			res.view( {
		  		mascotas: mascotas,
		  		persona: pers
		  	})
		}) 
	}
	
};

