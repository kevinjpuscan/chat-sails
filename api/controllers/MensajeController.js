/**
 * MensajeController
 *
 * @description :: Server-side logic for managing mensajes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	index: function (req, res, next) {// ...
		res.view( 'mensaje/index');
	},

	subscribe:  function (req, res) {
		if (!req.isSocket) {
	 	  return res.badRequest();
	 	}

	 	var room = 'chat';
		var username=req.param('user');
	 	sails.sockets.join(req, room, function(err) {
	 	  if (err) {
	 	    return res.serverError(err);
	 	  }

      sails.sockets.broadcast('chat', 'users', { user: username });
	 	  return res.json({
	 	    message: 'suscrito',
	 	  });
	 	});
	},

	list: function(req, res) {
		Mensaje.find( function mensajes (err, mensajes) {
			return res.json({
				mensajes: mensajes
			})
		})
	},

	publicar: function (req, res) {
    	var text = req.param('mensaje');
      var user = req.param('user');
		// Mensaje.create({
  //   	  texto: text;
  //   	});
    	sails.sockets.broadcast('chat', 'message', { alerta: text, user: user});
	},
  escribiendo:function (req,res){
	  var escribiendo=req.param('value');
    var user=req.param('user');
	  if(escribiendo){
      sails.sockets.broadcast('chat', 'status', { alerta: user+" está escribiendo..." });
    }else{
      sails.sockets.broadcast('chat', 'status', { alerta: "" });
    }
  }
};

