/**
 * Persona.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

  	nombres: {
      type: 'string'
    },

    apellidos: {
      type: 'string'
    },

    edad: {
      type: 'integer'
    },

    mascotas: {
      collection: 'mascota',
      via: 'dueño'
    }

  }
};

